<?php

class Post_Validation
{
  public $post_rules;
  public $post_default_val = array();
  public function __construct()
  {
      $this->post_rules = array("required","email","test");
  }
  public function post_validate($post){
    $err = array();
    foreach ($post as $key => $value) {
      $explode_dat = explode("|",$value["post_rules"]);
      for($x = 0;$x < count($explode_dat);$x++){
          $err[] = in_array($explode_dat[$x],$this->post_rules) ? $this->post_config($value) : die('Error imong post rule ondong...');
      }
    }
    $this->error_msg = $err;
    $count = 0;
    foreach ($err as $key => $value) {
      if(!empty($value)){
        $count++;
      }
    }
    return $count == 0 ? false : true;
  }
  public function post_config($value = array()){
    $explode_dat = explode("|",$value['post_rules']);
    for($x = 0;$x < count($explode_dat);$x++){
        if($explode_dat[$x] == "required"){
          if(empty($this->test_input($_POST[$value['post_name']]))){
              return array($value['post_name'] => $value['post_label']." is required");
          }
        }
        if($explode_dat[$x]  == "email"){
          $email = $this->test_input($_POST[$value['post_name']]);
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return array($value['post_name'] => $value['post_label']." is not valid");
          }
        }
    }
  }
  public function test_input($data="") {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  public function get_error($name_post){
      foreach($this->error_msg as $key => $value){
        if(isset($value[$name_post]))
          return $value[$name_post];
      }
  }
  public function set_error($name_post = ""){
    if(!empty($this->error_msg)){
        echo $this->get_error($name_post);
    }
  }
}

 ?>
